#### INSTRUKCJA INSTALACJI

W folderze themes/ zainstalowanego projektu Wordpress

```
git clone https://halinab@bitbucket.org/halinab/wordpress-starter-theme.git

cd wordpress-starter-theme

npm install

ustalenie proxy w webpack.config.js

```

Uruchomienie wersji developerskiej

```
npm run dev

```

Uruchomienie wersji produkcyjnej

```
npm run build

```

#### TABLE OF CONTENT - SNIPPETS

##### I. header_snip.php

1. REGISTER MENU
2. BREADCRUMBS

##### II. js-header_snip.js

1. SUBMENU SLIDE ON HOVER ON DESKTOP, CLICK ON MOBILE
2. PUSH MENU

Example: https://tympanus.net/Blueprints/SlidePushMenus/

    - Odkomentować fragment kodu w header.php, usunąć istniejący.
    - Skopiować kod js ze snippets/js_header_snip.js do main.js
    - Dodać do main.scss komponent navbar_push-menu, usunąć navbar_slide-menu.

##### III. search_snip.php

1. EXCLUDE PAGES FROM WORDPRESS SEARCH
2. CUSTOM POST TYPE SEARCH
3. CUSTOM SEARCH ACF
4. HALT THE MAIN QUERY IN THE CASE OF AN EMPTY SEARCH

##### IV. utils.php

1. POST NAME CHANGE
2. REGISTER CUSTOM POST TYPES
3. REGISTER CUSTOM TAXONOMY
4. RENAME DEFAULT CATEGORY TAXONOMY
5. CUSTOM EXCERPT
6. CUSTOM EXCERPT FUNCTION FOR ADVANCED CUSTOM FIELDS
7. ACF OPTION PAGE
8. CUSTOM IMAGES SIZES

##### V. js-utils.js

1. YOUTUBE PLAYER WITH CUSTOM COVER IMAGE
2. SYNC TWO OWL SLIDERS


###### Good luck!
