<?php

/* Template Name: News */

get_header(); ?>

<main>
  <section class="top-banner"
    style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/banner.jpg') no-repeat center center; background-size: cover;">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </section>
  <section class="content-section">
    <div class="container">
      <div class="row justify-content-center">
        <?php
        $news_query = new WP_Query(array(
          'orderby'      => 'post_date',
          'order'        => 'DESC',
          'post_status'  => 'publish',
          'paged' => $paged,
        ));
        if ($news_query->have_posts()) :
          ?>
        <?php while ($news_query->have_posts()) : $news_query->the_post(); ?>
        <div class="col-md-6 col-lg-4 d-flex mb-5">
          <article class="news">
            <a class="news__link" href="<?php the_permalink(); ?>">
              <div class="news__img"
                style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>')">
              </div>
            </a>
            <div class="news__content">
              <h2 class="news__title"><?php the_title(); ?></h2>
              <div class="news__excerpt"><?php the_excerpt(); ?></div>
              <a class="news__read-more" href="<?php the_permalink(); ?>">CZYTAJ DALEJ</a>
            </div>
          </article>
        </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      </div>
      <div class="pagination d-flex align-items-center">
        <?php
        $big = 999999999;
        echo paginate_links(array(
          'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
          'format' => '?paged=%#%',
          'current' => max(1, get_query_var('paged')),
          'total' => $news_query->max_num_pages,
          'mid_size'  => 2,
          'prev_text' => __('<span class="page-prev"></span>'),
          'next_text' => __('<span class="page-next"></span>'),
          'prev_next'          => true,
        ));
        ?>
      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>