function WHCreateCookie(name, value, days) {
  var date = new Date();
  date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
  var expires = "; expires=" + date.toGMTString();
  document.cookie = name + "=" + value + expires + "; path=/";
}

function WHReadCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

window.onload = WHCheckCookies;

function WHCheckCookies() {
  if (WHReadCookie("cookies_accepted") != "T") {
    var message_container = document.createElement("div");
    message_container.id = "cookies-message-container";
    var language = document.documentElement.getAttribute("lang");
    if (language == "pl-PL") {
      var cookieMessage =
        "Ta strona używa ciasteczek (cookies), dzięki którym nasz serwis może działać lepiej. ";
      var cookieLink = "http://wszystkoociasteczkach.pl";
      var cookieLinkMessage = "Dowiedz się więcej";
      var cookieButton = "Rozumiem";
    } else if (language == "en-US") {
      var cookieMessage =
        "This site uses cookies to provide the best experience possible. ";
      var cookieLink = "http://www.allaboutcookies.org";
      var cookieLinkMessage = "Learn more";
      var cookieButton = "Understood";
    } else {
      var cookieMessage =
        "Этот сайт использует куки (cookies), чтобы наш сервис мог работать лучше. ";
      var cookieLink = "https://ru.wikipedia.org/wiki/Cookie";
      var cookieLinkMessage = "Узнать больше";
      var cookieButton = "понимаю";
    }
    var html_code =
      '<div id="bottom-padding" style="height: 53px;"></div><div id="cookies-message" style="padding: 10px 15px; font-size: 14px; line-height: 22px; border-bottom: 1px solid #D3D0D0; text-align: center; position: fixed; bottom: 0px; background-color: #EFEFEF; width: 100%; z-index: 999;">' +
      cookieMessage +
      '<a class="cookie-link" style="color: grey;" href="' +
      cookieLink +
      '" target="_blank">' +
      cookieLinkMessage +
      '</a><a href="javascript:WHCloseCookiesWindow();" id="accept-cookies-checkbox" name="accept-cookies" style="background-color: #000; padding: 5px 10px; color: white; display: inline-block; margin-left: 10px; text-decoration: none; cursor: pointer;">' +
      cookieButton +
      "</a></div>";
    message_container.innerHTML = html_code;
    document.body.appendChild(message_container);
  }
}

function WHCloseCookiesWindow() {
  WHCreateCookie("cookies_accepted", "T", 365);
  $("#cookies-message").slideUp();
  $("#bottom-padding").slideUp();
}
