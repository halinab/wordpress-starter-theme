<?php
/**
 * The header for our theme
 *
 * @package Millenium_Studio_Theme
 */
?>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-12 footer__credentials">
			  <p>Copyrights. All rights reserved</p>
			  <a class="seo" title="projektowanie stron www" href="http://www.milleniumstudio.pl" target="_blank">Projektowanie stron www</a> <span style="cursor:pointer;" onclick="location.href = 'http://www.milleniumstudio.pl';">Millenium Studio</span>
			</div>
		</div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
