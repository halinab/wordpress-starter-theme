<?php get_header(); ?>

<main>
  <section class="top-banner"
    style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/banner.jpg') no-repeat center center; background-size: cover;">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1>Wyniki wyszukiwania dla: <?php the_search_query(); ?></h1>
        </div>
      </div>
    </div>
  </section>
  <section class="top-section news">
    <div class="container">
      <div class="row">

        <?php if(have_posts()) : ?>
        <?php while(have_posts()) : the_post(); ?>

        <div class="col-md-6 col-lg-3">
          <a href="<?php echo get_page_link(); ?>">
            <div class="news__img" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
            </div>
          </a>
          <div class='news__content-container'>
            <div class="news__content">
              <h3 class="news__title"><?php the_title(); ?></h3>
              <a class="news__read-more" href="<?php echo get_page_link(); ?>">więcej</a>
            </div>
          </div>
        </div>

        <?php endwhile; ?>

        <div class="pagination">
          <?php
          $big = 999999999;
          echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $news_query->max_num_pages,
            'mid_size'  => 2,
            'prev_text' => __( '<span class="page-prev"></span>'),
            'next_text' => __( '<span class="page-next"></span>'),
            'prev_next'          => true,
          ) );
        ?>
        </div>

        <?php wp_reset_postdata(); ?>
        <?php else : ?>

        <p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>

        <?php endif; ?>

      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>