<?php get_header(); ?>

<main class="fadein">
  <section class="top-slider-section">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1 class="main-title">
          <?php the_title(); ?>
          </h1>
        </div>
      </div>
    </div>
    <div class="top-slider owl-theme owl-carousel">
      <div class="top-slider__slide"
        style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/banner_1.jpg') no-repeat center center; background-size: cover;">
        <div class="container">
          <div class="row">
            <div class="col-md-6 offset-md-3 top-slider__content">
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Debitis dolore rem exercitationem sunt, asperiores dignissimos dolores</p>
            </div>
          </div>
        </div>
      </div>
      <div class="top-slider__slide"
        style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/banner_2.jpg') no-repeat center center; background-size: cover;">
        <div class="container">
          <div class="row">
            <div class="col-md-6 offset-md-3 top-slider__content">
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio commodi similique non atque unde aut ad hic nemo</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    <section class="news-slider-section mt-5">
    <div class="container">
      <div class="news-carousel owl-theme owl-carousel">
        <?php
         $news_query = new WP_Query(array(
            'orderby'      => 'post_date',
            'order'        => 'DESC',
            'post_status'  => 'publish',
            // 'posts_per_page' => 8
         ));
         if($news_query->have_posts()) :
         ?>
        <?php while($news_query->have_posts()) : $news_query->the_post(); ?>
        <article class="news-slider">
          <a class="news-slider__link" href="<?php the_permalink(); ?>">
            <div class="news-slider__img"
              style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium' ); ?>')">
            </div>
            <div class="news-slider__content">
              <h2 class="news-slider__title"><?php the_title(); ?></h2>
              <div class="news-slider__excerpt"><?php the_excerpt(); ?></div>
              <!-- <a class="news__read-more" href="<?php the_permalink(); ?>">więcej</a> -->
            </div>
          </a>
        </article>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <section class="articles-section mt-5">
    <div class="container">
      <div class="row justify-content-center">
        <?php
         $news_query = new WP_Query(array(
            'orderby'      => 'post_date',
            'order'        => 'DESC',
            'post_status'  => 'publish',
            'posts_per_page' => 3
         ));
         if($news_query->have_posts()) :
         ?>
        <?php while($news_query->have_posts()) : $news_query->the_post(); ?>
        <div class="col-md-6 col-lg-4 d-flex mb-5">
          <article class="news">
            <a class="news__link" href="<?php the_permalink(); ?>">
              <div class="news__img"
                style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium' ); ?>')">
              </div>
            </a>
            <div class="news__content">
              <h2 class="news__title"><?php the_title(); ?></h2>
              <div class="news__excerpt"><?php the_excerpt(); ?></div>
              <a class="news__read-more" href="<?php the_permalink(); ?>">CZYTAJ DALEJ</a>
            </div>
          </article>
        </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>