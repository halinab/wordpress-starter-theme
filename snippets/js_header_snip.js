//
// ─── TABLE OF CONTENT ───────────────────────────────────────────────────────────
//
//  1. SUBMENU SLIDE ON HOVER ON DESKTOP, CLICK ON MOBILE  
//  2. PUSH MENU 

//
// ─── 1. SUBMENU SLIDE ON HOVER ON DESKTOP, CLICK ON MOBILE ──────────────────────────
//

$(".menu-item-has-children > a").click(function(e) {
  e.preventDefault();
});
if (window.matchMedia("(max-width: 1400px)").matches) {
  $(".menu-item-has-children").click(function() {
    $(this)
      .find(".sub-menu")
      .stop()
      .slideToggle();
    $(this)
      .siblings()
      .find(".sub-menu")
      .slideUp();
  });
}
if (window.matchMedia("(min-width: 1400px)").matches) {
  $(".menu-item-has-children").hover(function() {
    $(this)
      .find(".sub-menu")
      .stop()
      .slideToggle();
  });
}

//
// ─── 2. PUSH MENU ──────────────────────────────────────────────────────────────────
//

$('.hamburger').click(function() {
  $('body').toggleClass('body-push');
  $('.navbar-links').toggleClass('menu-push');
});