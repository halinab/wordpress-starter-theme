<?php
/**
 * Millenium Studio Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Millenium_Studio_Theme
 */

//
// ─── TABLE OF CONTENT ───────────────────────────────────────────────────────────
//
// 1. POST NAME CHANGE 
// 2. REGISTER CUSTOM POST TYPES
// 3. REGISTER CUSTOM TAXONOMY 
// 4. RENAME DEFAULT CATEGORY TAXONOMY 
// 5. CUSTOM EXCERPT
// 6. CUSTOM EXCERPT FUNCTION FOR ADVANCED CUSTOM FIELDS
// 7. ACF OPTION PAGE
// 8. CUSTOM IMAGES SIZES
// 9. DISPLAY THE POSTS FROM A CUSTOM POST TYPE ON AN CATEGORY PAGE
 
//
// ─── 1. POST NAME CHANGE ───────────────────────────────────────────────────────────
//

function revcon_change_post_label()
{
  global $menu;
  global $submenu;
  $menu[5][0] = 'News';
  $submenu['edit.php'][5][0] = 'News';
  $submenu['edit.php'][10][0] = 'Add News';
  $submenu['edit.php'][16][0] = 'News Tags';
}
function revcon_change_post_object()
{
  global $wp_post_types;
  $labels = &$wp_post_types['post']->labels;
  $labels->name = 'News';
  $labels->singular_name = 'News';
  $labels->add_new = 'Add News';
  $labels->add_new_item = 'Add News';
  $labels->edit_item = 'Edit News';
  $labels->new_item = 'News';
  $labels->view_item = 'View News';
  $labels->search_items = 'Search News';
  $labels->not_found = 'No News found';
  $labels->not_found_in_trash = 'No News found in Trash';
  $labels->all_items = 'All News';
  $labels->menu_name = 'News';
  $labels->name_admin_bar = 'News';
}

add_action('admin_menu', 'revcon_change_post_label');
add_action('init', 'revcon_change_post_object');


//
// ─── 2. REGISTER CUSTOM POST TYPES ──────────────────────────────────────────────────────────
//

add_action('init', 'news_init_posttypes');

function news_init_posttypes()
{

  $news_args = array(
    'labels' => array(
      'name' =>  'Aktualności',
      'singular_name' => 'Aktualności',
      'all_items' => 'Wszystkie aktualności',
      'add_new' => 'Dodaj nową aktualność',
      'add_new_item' => 'Dodaj nową aktualność',
      'edit_item' => 'Edytuj aktualność',
      'new_item' => 'Nowa aktualność',
      'view_item' => 'Zobacz aktualność',
      'search_items' => 'Szukaj w aktualnościach',
      'not_found' => 'Nie znaleziono aktualności',
      'not_found_in_trash' => 'Brak aktualności w koszu',
      'parent_item_colon' => ''
    ),

    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite'  => array('slug' => 'aktualnosci', 'with_front' => false),
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_icon'           => 'http://example.milleniumhost.pl/wp-content/themes/...',
    'menu_position' => 5,
    'supports' => array(
      'title', 'editor', 'thumbnail'
    ),
    'has_archive' => true
  );
  register_post_type('news', $news_args);

  $galleries_args = array(
    'labels' => array(
      'name' => 'Galerie',
      'singular_name' => 'Galerie',
      'all_items' => 'Wszystkie galerie',
      'add_new' => 'Dodaj nową galerię',
      'add_new_item' => 'Dodaj nową galerię',
      'edit_item' => 'Edytuj galerię',
      'new_item' => 'Nowa galeria',
      'view_item' => 'Zobacz galerię',
      'search_items' => 'Szukaj galerii',
      'not_found' =>  'Nie znaleziono żadnych galerii',
      'not_found_in_trash' => 'Nie znaleziono żadnych galerii w koszu',
      'parent_item_colon' => ''
    ),
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite'  => array('slug' => 'galeria', 'with_front' => false),
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array(
      'title', 'editor',  'thumbnail'
    ),
    'has_archive' => true,

  );
  register_post_type('galleries', $galleries_args);
}

//
// ─── 3. REGISTER CUSTOM TAXONOMY ───────────────────────────────────────────────────
//

	
function custom_taxonomy() {

	$labels = array(
		'name'                       => 'Taxonomies',
		'singular_name'              => 'Taxonomy',
		'menu_name'                  => 'Taxonomy',
		'all_items'                  => 'All Items',
		'parent_item'                => 'Parent Item',
		'parent_item_colon'          => 'Parent Item:',
		'new_item_name'              => 'New Item Name',
		'add_new_item'               => 'Add New Item',
		'edit_item'                  => 'Edit Item',
		'update_item'                => 'Update Item',
		'view_item'                  => 'View Item',
		'separate_items_with_commas' => 'Separate items with commas',
		'add_or_remove_items'        => 'Add or remove items',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Items',
		'search_items'               => 'Search Items',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No items',
		'items_list'                 => 'Items list',
		'items_list_navigation'      => 'Items list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_in_rest'               => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'taxonomy_name', array( 'citybreaks' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );

//
// ─── 4. RENAME DEFAULT CATEGORY TAXONOMY ───────────────────────────────────────────
//


function revcon_change_cat_label() {
	global $submenu;
	$submenu['edit.php'][15][0] = 'Travel Guide'; 
}
add_action( 'admin_menu', 'revcon_change_cat_label' );

//
// ─── 5. CUSTOM EXCERPT ─────────────────────────────────────────────────────────────
//

function the_excerpt_max_charlength($charlength)
{
	global $post;
	$excerpt = get_the_excerpt();
	$charlength++;

	if (mb_strlen($excerpt) > $charlength) {
		$subex = mb_substr($excerpt, 0, $charlength - 5);
		$exwords = explode(' ', $subex);
		$excut = -(mb_strlen($exwords[count($exwords) - 1]));
		if ($excut < 0) {
			echo mb_substr($subex, 0, $excut);
		} else {
			echo $subex;
		}
		echo '<a class="excerpt-link" href="' . get_permalink($post->ID) . '">' . 'Read More &raquo;' . '</a>';
	} else {
		echo $excerpt;
	}
}


//
// ─── 6. CUSTOM EXCERPT FUNCTION FOR ADVANCED CUSTOM FIELDS ─────────────────────────
//

function custom_field_excerpt()
{
  global $post;
  $text = get_field('desc'); //Replace 'your_field_name'
  if ('' != $text) {
    $text = strip_shortcodes($text);
    $text = apply_filters('the_content', $text);
    $text = str_replace(']]&gt;', ']]&gt;', $text);
    $excerpt_length = 20; //  words
    $excerpt_more = apply_filters('excerpt_more', ' ' . '...');
    $text = wp_trim_words($text, $excerpt_length, $excerpt_more);
  }
  return apply_filters('the_excerpt', $text);
}


//
// ─── 7. ACF OPTION PAGE ────────────────────────────────────────────────────────────
//

if (function_exists('acf_add_options_page')) {
  acf_add_options_page('Options');
  acf_add_options_page('More Theme Settings');
}


//
// ─── 8. CUSTOM IMAGES SIZES  ──────────────────────────────────────
//

function device_image_sizes() {
	add_image_size( 'mobile-size', 375, 667, true );
	add_image_size( 'tablet-size', 768, 1024, true );
}


function show_device_at_img_select($sizes) {
	$sizes['iphone-size'] = __( 'Image size for iphone' );
	$sizes['tablet-size'] = __( 'image size for tablet' );

	return $sizes;
}
add_action( 'init', 'device_image_sizes' );
add_filter('image_size_names_choose', 'show_device_at_img_select');

// the_post_thumbnail( 'tablet-size' ); ---> CODE TO TRIGGER FUNCTION





