<?php

//
// ─── TABLE OF CONTENT ───────────────────────────────────────────────────────────
//
//  1. REGISTER MENU 
//  2. BREADCRUMBS
  



//
// ─── 1. REGISTER MENU ──────────────────────────────────────────────────────────────
//

function register_my_menu()
{
  register_nav_menus(
    array('primary-nav' => __('Primary Menu'))
  );
}
add_action('init', 'register_my_menu');

//
// ─── 2. BREADCRUMBS ────────────────────────────────────────────────────────────────
//

// HOME > CUSTOM POST TYPE NAME > CUSTOM TAXONOMY NAME > POST NAME
?>

<?php
$cpt = get_post_type_object('citybreaks');  // example for citybreaks
$terms = get_the_terms($post->ID, 'citybreaks_taxonomy');
?>

<div class="breadcrumbs">
  <a href="<?php echo get_home_url(); ?>">Home</a> >
  <a href="<?php echo get_post_type_archive_link('citybreaks'); ?>"><?php echo $cpt->labels->name; ?></a> >
  <a href="<?php echo get_term_link($terms[0]); ?>"><?php echo $terms[0]->name; ?></a> >
  <strong><?php the_title(); ?></strong>
</div>

<!-- HOME > PARENT CATEGORY NAME > CHILD CATEGORY NAME > POST NAME -->

<?php
$category = get_the_category();
$category_parent_id = $category[0]->category_parent;
$category_parent = get_term($category_parent_id, 'category'->name);
$child_category = $category[0]->cat_name;
$child_category_id = $category[0]->cat_ID;
$child_category_link = get_category_link($child_category_id);
?>

<div class="breadcrumbs">
  <a href="<?php echo get_home_url(); ?>">Home</a> >
  <a href="<?php echo get_category_link($category_parent_id); ?>"><?php echo $parent_category; ?></a> >
  <a href="<?php echo $child_ctegory_link; ?>"><?php echo $child_category; ?></a> >
  <strong><?php the_title(); ?></strong>
</div>