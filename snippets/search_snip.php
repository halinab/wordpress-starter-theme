<?php

//
// ─── TABLE OF CONTENT ───────────────────────────────────────────────────────────
//
// 1. EXCLUDE PAGES FROM WORDPRESS SEARCH
// 2. CUSTOM POST TYPE SEARCH
// 3. CUSTOM SEARCH ACF
// 4. HALT THE MAIN QUERY IN THE CASE OF AN EMPTY SEARCH

//
// ─── 1. EXCLUDE PAGES FROM WORDPRESS SEARCH ────────────────────────────────────────
//

if (!is_admin()) {
  function wpb_search_filter($query)
  {
    if ($query->is_search) {
      $query->set('post_type', 'post');
    }
    return $query;
  }
  add_filter('pre_get_posts', 'wpb_search_filter');
}


//
// ─── 2. CUSTOM POST TYPE SEARCH ────────────────────────────────────────────────────
//

function searchfilter($query)
{
  if ($query->is_search) {
    $query->set('post_type', array('products', 'post'));
  }
  return $query;
}
add_filter('pre_get_posts', 'searchfilter');

//
// ─── 3. CUSTOM SEARCH ACF ──────────────────────────────────────────────────────────
//

/**
 * Extend WordPress search to include custom fields
 * http://adambalee.com
 *
 * Join posts and postmeta tables
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
	global $wpdb;
	if ( is_search() ) {    
			$join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	}
	return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
* Modify the search query with posts_where
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
*/
function cf_search_where( $where ) {
	global $pagenow, $wpdb;
	if ( is_search() ) {
			$where = preg_replace(
					"/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
					"(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
	}
	return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
* Prevent duplicates
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
*/
function cf_search_distinct( $where ) {
	global $wpdb;
	if ( is_search() ) {
			return "DISTINCT";
	}
	return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );
