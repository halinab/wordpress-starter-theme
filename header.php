<?php
/**
 * The header for our theme
 *
 * @package Millenium_Studio_Theme
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900&amp;subset=latin-ext"
    rel="stylesheet">
  <link rel="shortcut icon" type="image/x-icon"
    href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.ico">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <header>
    <div class="container">

      <!--------- RIGHT PUSH MENU => Instruction => README -------->

      <!-- <nav class="navbar">
        <a href="/" class="navbar-brand">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="Logo">
        </a>
        <button class="hamburger hamburger--spin-r" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
        <div class="navbar-links"> -->
        
        <nav class="navbar navbar-expand-lg nav">
          <a href="/" class="navbar-brand">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="Logo">
          </a>
          <button class="navbar-toggler hamburger hamburger--spin-r" type="button" data-toggle="collapse"
            data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Rozwiń nawigację">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button>
          <div class="collapse navbar-collapse" id="mainNav">

          <?php
					 wp_nav_menu( array(
							// 'theme_location' => 'primary-nav',
							'container' => 'ul',
							'menu_class' => 'navbar-nav ml-auto'
					 ) );
           ?>

          <!--------- WPML Widget ---------->

          <!-- <?php //do_action('wpml_add_language_selector'); ?> -->

          <div class="hd-search">
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
              <label>
                <span class="screen-reader-text">Szukaj:</span>
                <input type="search" class="search-field" placeholder="Szukaj &hellip;" value="" name="s" />
              </label>
              <button type="submit" class="search-submit">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                  viewBox="0 0 129 129" enable-background="new 0 0 129 129" width="512px" height="512px">
                  <path
                    d="M51.6,96.7c11,0,21-3.9,28.8-10.5l35,35c0.8,0.8,1.8,1.2,2.9,1.2s2.1-0.4,2.9-1.2c1.6-1.6,1.6-4.2,0-5.8l-35-35   c6.5-7.8,10.5-17.9,10.5-28.8c0-24.9-20.2-45.1-45.1-45.1C26.8,6.5,6.5,26.8,6.5,51.6C6.5,76.5,26.8,96.7,51.6,96.7z M51.6,14.7   c20.4,0,36.9,16.6,36.9,36.9C88.5,72,72,88.5,51.6,88.5c-20.4,0-36.9-16.6-36.9-36.9C14.7,31.3,31.3,14.7,51.6,14.7z" />
                </svg>
              </button>
            </form>
          </div>
        </div>
      </nav>
    </div>
  </header>